import PropTypes from "prop-types";

import { NewsletterList } from "./NewsletterList";

const Home = ({ newsletters }) =>
  newsletters.map((newsletter, index) => (
    <NewsletterList key={index} {...newsletter} />
  ));

Home.propTypes = {
  newsletters: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
    })
  ),
};

export { Home };
