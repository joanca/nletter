import PropTypes from "prop-types";

const NewsletterList = ({ id, title, description }) => (
  <article key={`${id}`}>
    <a href={`/newsletter/${id}`}>
      <h2>{title}</h2>
    </a>
    <p>{description}</p>
  </article>
);

NewsletterList.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

export { NewsletterList };
