import PropTypes from "prop-types";

import styles from "./Layout.module.scss";

const Layout = ({ children }) => (
  <main className={styles.wrapper}>{children}</main>
);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export { Layout };
