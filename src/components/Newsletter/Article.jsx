import PropTypes from "prop-types";

const Article = ({ uuid, title, description, url }) => (
  <article key={`${uuid}`}>
    <a href={url}>
      <h2>{title}</h2>
    </a>
    <p>{description}</p>
  </article>
);

Article.propTypes = {
  uuid: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

export { Article };
