import PropTypes from "prop-types";

import { Article } from "./Article";

import { useRouter } from "next/router";

const Newsletter = ({ articles }) => {
  const router = useRouter();

  if (router.isFallback) return "loading...";

  return articles.map((article, index) => <Article key={index} {...article} />);
};

Newsletter.propTypes = {
  articles: PropTypes.arrayOf(
    PropTypes.shape({
      uuid: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
    })
  ).isRequired,
};

export { Newsletter };
