const SHEET_URL = `${process.env.SHEETS_BASE_URL}/${process.env.SHEETS_ID}`;

const getAllNewsletters = async () => {
  const newsletters = await fetchAllNewsletters();

  return parseNewslettersList(newsletters);
};

const getNewsletter = async (id) => {
  const data = await fetchNewsletterArticles(sheetUrl(id));

  return {
    code: data.code,
    title: id,
    articles: parseNewsletter(data.values),
  };
};

const fetchAllNewsletters = async () => {
  const url = `${SHEET_URL}?key=${process.env.SHEETS_API_KEY}`;
  const request = await fetch(url);
  const result = await request.json();

  const newsletterPromises = result.sheets.map(async (sheet) =>
    fetchNewsletter(sheetUrl(sheet.properties.title))
  );

  const newsletterTitles = result.sheets.map((sheet) => sheet.properties.title);

  const newsletters = await Promise.all(newsletterPromises);

  return { newsletters, titles: newsletterTitles };
};

const fetchNewsletter = async (url) => {
  const request = await fetch(url);
  const result = await request.json();

  return {
    code: request.status,
    values: result.values,
  };
};

const fetchNewsletterArticles = async (url) => {
  const request = await fetch(url);
  const result = await request.json();

  return {
    code: request.status,
    values: result.values.slice(6),
  };
};

const parseNewslettersList = ({ newsletters, titles }) =>
  newsletters.map((newsletter, index) => ({
    id: titles[index],
    title: newsletter.values[2][0],
    image: newsletter.values[2][1],
    description: newsletter.values[2][2],
    date: newsletter.values[2][3],
  }));

const parseNewsletter = (values) =>
  values.map((entry) => ({
    uuid: entry[0],
    title: entry[1],
    url: entry[2],
    description: entry[3],
    image: entry[4],
    date: entry[5],
  }));

const sheetUrl = (sheetId = null) =>
  `${SHEET_URL}/values/${sheetId}?key=${process.env.SHEETS_API_KEY}`;

export { getAllNewsletters, getNewsletter };
