# nletter
Make your newsletters from google spreadsheets and publish them in a static site.



## Development
To run the project, first install the dependencies: `yarn` (or `npm i`).

Then, to run locally, run: `yarn dev` (or `npm run dev`). The project will be available at `http://localhost:5001/`.