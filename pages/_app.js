import Head from "next/head";
import PropTypes from "prop-types";

import { Layout } from "src/components/Layout/Layout";
import "src/styles.scss";

const App = ({ Component, pageProps }) => (
  <>
    <Head>
      <title>{pageProps.title}</title>
      <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap"
        rel="stylesheet"
      />
    </Head>
    <Layout>
      <Component {...pageProps} />
    </Layout>
  </>
);

App.propTypes = {
  Component: PropTypes.func.isRequired,
  pageProps: PropTypes.shape({
    title: PropTypes.string.isRequired,
  }).isRequired,
};

export default App;
