import { Home } from "src/components/Home/Home";
import { getAllNewsletters } from "src/services/newsletterService";

export const getStaticProps = async () => {
  const newsletters = await getAllNewsletters();

  return {
    props: {
      title: "newsletters",
      newsletters,
    },
    unstable_revalidate: process.env.REVALIDATE,
  };
};

export default Home;
