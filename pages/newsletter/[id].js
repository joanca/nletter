import { Newsletter } from "src/components/Newsletter/Newsletter";

import {
  getAllNewsletters,
  getNewsletter,
} from "src/services/newsletterService";

export const getStaticPaths = async () => {
  const newsletters = await getAllNewsletters();

  const paths = newsletters.map((newsletter) => ({
    params: { id: String(newsletter.id) },
  }));

  return { paths, fallback: true };
};

export const getStaticProps = async ({ params }) => {
  const newsletter = await getNewsletter(params.id);

  return {
    props: {
      title: newsletter.title,
      articles: newsletter.articles,
    },
    unstable_revalidate: process.env.REVALIDATE,
  };
};

export default Newsletter;
